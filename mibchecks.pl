#!/usr/bin/perl

# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

my $VERSION = "1.0.2";
my $snmpdcfg = "/etc/snmp/snmpd.conf";

sub extenddec {
    my ($extend) = (@_);
    $a = '';
    foreach $x (split(//, $extend)) {
	$a .= sprintf(".%s", ord($x));
    }
    return $a;
}


if ( ! -f $snmpdcfg ) {
    printf "file not found : %s, exiting\n", $snmpdcfg;
    exit 1;
}

printf "# Automatically generated by mibchecks %s\n", $VERSION;
print "# https://gitorious.org/mibchecks\n#\n\n";

open(FP, "<$snmpdcfg");

while (<FP>) {

    unless (/^#/) {

	$comm = $1 if /rocommunity (\w*)/;
	
	if (/extend\s+(\w*)/) {

	    $extend = $1;
	    $oid = extenddec($extend);

	    printf <<EOF, $extend, $oid, $extend, $oid, $extend;
# extend : %s
# oid : .1.3.6.1.4.1.8072.1.3.2.4.1.2.8%s
#
#
define command {
    command_name    snmp_mib_%s
    command_line    /usr/lib/nagios/plugins/check_snmp -H '\$HOSTADDRESS\$' -C '\$ARG1\$' -o .1.3.6.1.4.1.8072.1.3.2.4.1.2.8%s.1 -w :'\$ARG2\$' -c :'\$ARG3\$' -l %s
}

EOF

	}
    }
}

close(FP);
